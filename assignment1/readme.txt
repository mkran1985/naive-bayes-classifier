Readme File - Assignment 1
Author: Muthukumaran Chandrasekaran
Naive Bayes Classifier
Current Version: v0.5
------------------------------------


------------------------------------
Naive Bayes Classifier - v0.5
------------------------------------



Results: 
---------
Output of the NB Classifier: /Out/AfterClassifier/part-r-00000.txt
0	0	[CCAT,MCAT]	GCAT	-14910.638434075143
7481	0	[ECAT,GCAT]	CCAT	-5247.48751814148
10762	1	[CCAT]	CCAT	-1243.0852867007964
11695	0	[GCAT]	CCAT	-2304.7067014534364
12995	1	[CCAT,GCAT]	CCAT	-941.5094076353987
13557	1	[CCAT,MCAT]	CCAT	-1793.5515744443283
14856	1	[CCAT,ECAT,MCAT]	ECAT	-2732.2922872529502
16802	1	[CCAT]	CCAT	-2934.8051037393498

Console Output:
Testing Results
---------------------------
Correctly Classified: 	5
Total Documents: 	8
Percent Correct: 	5/8 = 62.5%


Update for the classification job.
[Ref: White board discussions with Manish]
[Ref: Lecture 3 Slides for the prior probability formula]
[Ref: http://diveintodata.org/2011/03/15/an-example-of-hadoop-mapreduce-counter/]
[Ref: https://spectrallyclustered.wordpress.com/author/magsol/page/3/]


1. Wrote mapper and reducer for computing the most likely label for each test document and compared the classification to the true labels on the test data.
2. Tested it on the very small data set.
3. Modified the NBmain function to print out the final classifier accuracy. 


------------------------------------
Naive Bayes Classifier - v0.4
------------------------------------
Update for preprocessing before classification Job

1. Modified the mappers and reducers for joining the testing and training data. 
[Ref: White board discussions with Roi Ceren]
2. Added a reducer function called NBJoinReduce.java.
3. Created placeholders for the mapper and reducer for the actual classification
4. Modified NBmain class to take in the output of the join job and call the classifier job

Example Output of the Join Job:

Key (Word)	<Training: Labels-Counts> | <Testing: [DocID::Labels:Word Freq in Doc]#[...]#[...]> 
....
December	CCAT-5,MCAT-1|7481::ECAT,GCAT:1
Department	CCAT-1,ECAT-1,GCAT-3|7481::ECAT,GCAT:1
Desk		ECAT-1,MCAT-1|14856::CCAT,ECAT,MCAT:1
Europe		MCAT-1|14856::CCAT,ECAT,MCAT:1#16802::CCAT:3
....





------------------------------------
Naive Bayes Classifier - v0.3
------------------------------------
Update for collecting collecting Test Stats and Preprocessing before actual classification

1. Added a Mapper (NBtestingMap) and a reducer (NBtestingReduce) for counting test data stats
2. Added an Identity Mapper (DoNothingMapper) and a reducer (NBJoinReduce) for joining the results from test and training data on the key "Word".
3. Updated the NBmain class to include 2 additional jobs - one for collecting Testing stats and one for "joining" Testing and Training stats.
4. Output is all the preprocessing we ever needed before the "Classification" Job. 

Details:

1a. NBtestingMap Class
*************************
This is the mapper file takes as input the testing documents and collects a <K,V> = <Word, [Document ID, Labels associated with Word]>

//Mapper Function
1a. public class NBtestingMap extends Mapper<LongWritable, Text, Text, Text>

1b. NBtestingReduce Class
*************************
//Each reducer instance will receive <k,V> i.e. <label,[Doc ID & Labels associated w/ word for that document]> after the mappers have completed.

//Reducer Function
1b. public class NBtestingReduce extends Reducer<Text, Text, Text, Text>

2a. NBDoNothingMapper Class
*************************
This is an identity mapper file (called twice, one for training and one for testing) that takes as input, the outputs of a reducer (NBTrainingReduce or NBtestingReduce) and returns a <K,V> pair with each word as the key.

//Mapper Function
2a. public class NBDoNothingMapper extends Mapper<Text, Text, Text, Text>

2b. NBJoinReduce Class
*************************
// Each reducer instance will the joins the output of testing and training reducers key'ed on the "word"ment]> after the mappers have completed.

//Reducer Function
2b. public class NBJoinReduce extends Reducer<Text, Text, Text, Text>






------------------------------------
Naive Bayes Classifier - v0.2
------------------------------------
Update for collecting Label Stats...Summary

1. Added a Mapper (NBLabelStatsMap) and Reducer (NBLabelStatsReduce) class for counting Label Statistics
2. Updated the NBmain class to add a new job (labelStatsJob) to execute the above mapper/reducer classes

Details:

1a. NBLabelStatsMap Class
*************************
This is the mapper file takes as input the training document and collects a <K,V> = <Label, #Words in that Label>

//Mapper Function
[Reference] Black-board Discussions with Roi Ceren and Will Richardson
1. public void map(LongWritable key, Text value, Context context)

1b. NBLabelStatsReduce Class
*************************
//Each reducer instance will receive <k,V> i.e. <label,#words in the label> after the mappers have completed
//1. So we can update the global counter for total unique labels.
//2. We iterate over the values and count how many documents (numDocs) appear corresponding to the key (label).
//3. Each time we increment numDocs, we can increment the global counter for total number of documents also.
//Finally, we write out sumWPL (i.e. Y=y and W=*) and numDocs (i.e. Y=y)

Other Input parameters: 
1. Number of reducer tasks (i.e. we can set it: labelStatsJob.setNumReduceTasks(1);)

Functions: 

//Reducer Function
[Reference] Black-board Discussions with Roi Ceren and Will Richardson
1. public void reduce(Text key, Iterable<IntWritable> val, Context context)






------------------------------------
Naive Bayes Classifier - v0.1
------------------------------------

1. NBmain Class
****************
This is the driver file that manages the sequence of mapper/reducer jobs

Global Counters: 
[Reference] http://diveintodata.org/2011/03/15/an-example-of-hadoop-mapreduce-counter/
1. totDocs, //(Y=*)
2. totUniqueWords, //We need this for keeping track of frequencies associated with each unique word
3. totUniqueLabels  //we need this for testing (for iterating through all labels, we need to know how many unique labels there are)

Other Input parameters: 
1. Number of reducer tasks (i.e. we can set it: trainingJob.setNumReduceTasks(1);)

Functions: 

//Function to remove non-word characters from array of words 
//(2 token onwards after removing double spaces) and convert to vector of words 
[Reference] Assignment1.pdf
1. public static Vector<String> removeNonWords(String[] wArr)

//Function to convert label string (first token after removing double spaces) into vector of labels
2. public static Vector<String> removeCommas(String yStr)

//Main function takes in 2 input arguments (for now) - 
//1. Input directory (directory where the training data is located) and 
//2. Output directory (directory where the results of the reducers are stored

2. NBTrainingMap Class 
***********************
//this extends the mapper class

Functions: 
//Function that defines the first mapper. 
1. public void map(LongWritable key, Text value, Context context)

//Each mapper instance receives a document, containing labels and words associated with them.
//First we convert the document (i.e., a line) into useful words
//Then we remove commas from the 1st token which will separate out the labels associated with the document. 
//Only the labels that have "CAT" in them are considered useful labels 
//Finally, we write out <key,value> pairs which in our case are <word,label> pairs
//Effectively, we now have all pairs of the kind (W = w, Y = y)

3. NBTrainingReduce Class 
**************************
//this extends the reduce class

Functions: 
//Function that defines the first reducer. 
1. public void reduce(Text key, Iterable<Text> values, Context context)

[Reference] The pseudo code from Assignment1.pdf

//Each reducer will receive all <key,value> pairs associated with a particular key (which is a word, in our case)
//So we can update the global counter for total unique words by incrementing it everytime a reducer is called
//Next, for each word, we calculate the number of times it appeared under each unique label 
//the term 'k' refers to the total number of useful and unique labels for the correcponding document
//We then write the result out in the following form:  WORD	Label1-COUNT,Label2-COUNT,...,Labelk-COUNT


Trial Run Results: 
*******************
Input file: Train/RCV1.very_small_train.txt
Output folder: Out/

Global Counter: 
totUniqueWords=4331

Parameters:
setNumReduceTasks(r) //r = {0,5,10}

Output file snippet:
...
communications	CCAT-2,ECAT-2,
community	GCAT-1,
companies	CCAT-11,ECAT-2,MCAT-1,GCAT-2,
...
