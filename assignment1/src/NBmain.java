import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.protocol.NSQuotaExceededException;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */

public class NBmain {
	
	// Initializing Global counters
	// Ref: http://diveintodata.org/2011/03/15/an-example-of-hadoop-mapreduce-counter/
		  
	  static enum GlobalCounters {
	        totDocs, //(Y=*)
	        totUniqueWords, //We need this for keeping track of frequencies associated with each unique word
	        totUniqueLabels  //we need this for testing (for iterating through all labels, we need to know how many unique labels there are)
	  };
	  
	  public static final String totDocs = "totDocs";
	  public static final String totUniqueWords = "totUniqueWords";
	  public static final String totUniqueLabels = "totUniqueLabels";
	  
	  //Function to remove non-word characters from array of words 
	  //(2 token onwards after removing double spaces) and convert to vector of words 
	  //(inspired from assignment1.pdf)

	  public static Vector<String> removeNonWords(String[] wArr) {
//		  String[] words = cur_doc.split("\\s+");
		  Vector<String> wVec = new Vector<String>();
		  for (int i = 1; i < wArr.length; i++) {
			  wArr[i] = wArr[i].replaceAll("\\W", "");
			  if (wArr[i].length() > 0) {
				  wVec.add(wArr[i]);
			  }
		  } 
		  return wVec;
	  }
	  
	//Function to convert label string (first token after removing double spaces) into vector of labels
	  
	public static Vector<String> removeCommas(String yStr) {
		String [] yArr = yStr.split(",");
		Vector<String> yVec = new Vector<String>();
		for (String y : yArr) {
		    yVec.add(y);
		}
		return yVec;
	}


  @SuppressWarnings("deprecation")
public static void main(String[] args) throws Exception {
	  
	 /*
     * Validate that three arguments were passed from the command line.
     */
    if (args.length != 4) {
      System.out.printf("Usage: NBmain <path to training data> <path to testing data> <path to output dir> <# Reducer Tasks>\n");
      System.exit(-1);
    }
    
    Path trainingDataPath = new Path(args[0]);
    Path testingDataPath = new Path(args[1]);
    Path outputDirPath = new Path(args[2]);
    int numOfReducers = Integer.parseInt(args[3]);
    
    Path trainingStatsPath = new Path(outputDirPath, "Stats/TrainingStats");
    Path labelStatsPath = new Path(outputDirPath, "Stats/LabelStats");
    Path testStatsPath = new Path(outputDirPath, "Stats/TestingStats");
    Path afterJoinPath = new Path(outputDirPath, "AfterJoin");
    Path afterClassifierPath = new Path(outputDirPath, "AfterClassifier");
    // ---------------Getting Training Stats-----------------
    
    /*
     * Instantiate a Job object for your job's configuration. 
     */
    Configuration conf = new Configuration();
    Configuration conf2 = new Configuration();
    
    FileSystem outFolder = trainingStatsPath.getFileSystem(conf);
    outFolder.delete(trainingStatsPath, true);    
    Job trainingJob = new Job(conf, "NBTrainingJob");
    
    trainingJob.setJarByClass(NBmain.class);   
    trainingJob.setMapperClass(NBtrainingMap.class);
    trainingJob.setReducerClass(NBtrainingReduce.class);
    trainingJob.setNumReduceTasks(numOfReducers);
    
    trainingJob.setInputFormatClass(TextInputFormat.class);
    trainingJob.setOutputFormatClass(TextOutputFormat.class);
    
    trainingJob.setOutputKeyClass(Text.class);
    trainingJob.setOutputValueClass(Text.class);

    trainingJob.setMapOutputKeyClass(Text.class);
    trainingJob.setMapOutputValueClass(Text.class);
    
    FileInputFormat.addInputPath(trainingJob, trainingDataPath);
    FileOutputFormat.setOutputPath(trainingJob, trainingStatsPath);
    
    if (!trainingJob.waitForCompletion(true)) {
        System.err.println("training failed!");
        System.exit(1);
    }
    
    conf2.setLong(NBmain.totUniqueWords,trainingJob.getCounters().findCounter(NBmain.GlobalCounters.totUniqueWords).getValue());
    

    // ---------------Getting Label Stats-----------------
    
    FileSystem outFolder2 = labelStatsPath.getFileSystem(conf);
    outFolder2.delete(labelStatsPath, true);
    
    Job labelStatsJob = new Job(conf, "NBLabelStatsJob");
    
	labelStatsJob.setJarByClass(NBmain.class);   
	labelStatsJob.setMapperClass(NBLabelStatsMap.class);
	labelStatsJob.setReducerClass(NBLabelStatsReduce.class);
	labelStatsJob.setNumReduceTasks(numOfReducers);
	    
	labelStatsJob.setInputFormatClass(TextInputFormat.class);
	labelStatsJob.setOutputFormatClass(TextOutputFormat.class);
	    
	labelStatsJob.setOutputKeyClass(Text.class);
	labelStatsJob.setOutputValueClass(Text.class);
	
	labelStatsJob.setMapOutputKeyClass(Text.class);
	labelStatsJob.setMapOutputValueClass(IntWritable.class);
  
    FileInputFormat.addInputPath(labelStatsJob, trainingDataPath);
    FileOutputFormat.setOutputPath(labelStatsJob, labelStatsPath);

    if (!labelStatsJob.waitForCompletion(true)) {
        System.err.println("label stats failed!");
        System.exit(1);
    }
    
     
    conf2.setLong(NBmain.totUniqueLabels,labelStatsJob.getCounters().findCounter(NBmain.GlobalCounters.totUniqueLabels).getValue());
    conf2.setLong(NBmain.totDocs,labelStatsJob.getCounters().findCounter(NBmain.GlobalCounters.totDocs).getValue());
    
    
    //-------------Collect Testing Stats---------------    
   
	FileSystem outFolder3 = testStatsPath.getFileSystem(conf);
    outFolder3.delete(testStatsPath, true);    
    Job testingJob = new Job(conf, "NBTestingJob");
    
    testingJob.setJarByClass(NBmain.class); 
    testingJob.setNumReduceTasks(numOfReducers);
    
    testingJob.setMapperClass(NBtestingMap.class);
    testingJob.setReducerClass(NBtestingReduce.class);
    
    testingJob.setInputFormatClass(TextInputFormat.class);
    testingJob.setOutputFormatClass(TextOutputFormat.class);
    
    testingJob.setOutputKeyClass(Text.class);
    testingJob.setOutputValueClass(Text.class);

    testingJob.setMapOutputKeyClass(Text.class);
    testingJob.setMapOutputValueClass(Text.class);
    
    FileInputFormat.addInputPath(testingJob, testingDataPath);
    FileOutputFormat.setOutputPath(testingJob, testStatsPath);
    
    if (!testingJob.waitForCompletion(true)) {
        System.err.println("Testing failed!");
        System.exit(1);
    }
    
    // Now, Join Training stats with Testing stats on key - "word"   
    // Ref: http://kickstarthadoop.blogspot.com/2011/09/joins-with-plain-map-reduce.html
    
	FileSystem outFolder4 = afterJoinPath.getFileSystem(conf);
    outFolder4.delete(afterJoinPath, true);    
    Job reduceJoinJob = new Job(conf, "NBReduceJoinJob");
    
    reduceJoinJob.setJarByClass(NBmain.class); 
    reduceJoinJob.setNumReduceTasks(numOfReducers);
    
    MultipleInputs.addInputPath(reduceJoinJob, trainingStatsPath, KeyValueTextInputFormat.class, NBDoNothingMapper.class);
    MultipleInputs.addInputPath(reduceJoinJob, testStatsPath, KeyValueTextInputFormat.class, NBDoNothingMapper.class);
    
    reduceJoinJob.setReducerClass(NBJoinReduce.class);
    
    reduceJoinJob.setOutputFormatClass(TextOutputFormat.class);
    
    reduceJoinJob.setOutputKeyClass(Text.class);
    reduceJoinJob.setOutputValueClass(Text.class);

    reduceJoinJob.setMapOutputKeyClass(Text.class);
    reduceJoinJob.setMapOutputValueClass(Text.class);
    
    FileOutputFormat.setOutputPath(reduceJoinJob, afterJoinPath);
    
    if (!reduceJoinJob.waitForCompletion(true)) {
        System.err.println("Join failed!");
        System.exit(1);
    } 
    
   
    //Final Classification Job
    
    FileSystem filesys = labelStatsPath.getFileSystem(conf2);
    Path path = new Path(labelStatsPath, "part-r-[0-9]*");
    FileStatus [] fStatus = filesys.globStatus(path);
    for (FileStatus i : fStatus) {
        DistributedCache.addCacheFile(i.getPath().toUri(), conf2);
    }
    
    FileSystem outFolder5 = afterClassifierPath.getFileSystem(conf2);
    outFolder5.delete(afterClassifierPath, true);    
    Job classifierJob = new Job(conf2, "NBClassifierJob");
    
    classifierJob.setJarByClass(NBmain.class); 
    classifierJob.setNumReduceTasks(numOfReducers);
    
    
    classifierJob.setMapperClass(NBclassifierMap.class);
    classifierJob.setReducerClass(NBclassifierReduce.class);
    
    classifierJob.setInputFormatClass(KeyValueTextInputFormat.class);
    classifierJob.setOutputFormatClass(TextOutputFormat.class);
    
    classifierJob.setOutputKeyClass(LongWritable.class);
    classifierJob.setOutputValueClass(Text.class);

    classifierJob.setMapOutputKeyClass(LongWritable.class);
    classifierJob.setMapOutputValueClass(Text.class);
    
    FileInputFormat.addInputPath(classifierJob, afterJoinPath);
    FileOutputFormat.setOutputPath(classifierJob, afterClassifierPath);
    
    if (!classifierJob.waitForCompletion(true)) {
        System.err.println("Classification failed!");
        System.exit(1);
    } 
    
    int flag = 0;
    int totDocs = 0;
    path = new Path(afterClassifierPath, "part-r-[0-9]*");
    FileStatus [] files = filesys.globStatus(path);
    for (FileStatus fs : files) {
        FSDataInputStream inp = filesys.open(fs.getPath());
        BufferedReader i = new BufferedReader(new InputStreamReader(inp));
        String lnStr;
        while ((lnStr = i.readLine()) != null) {
            String [] lnStrArr = lnStr.split("\t");
            if (lnStrArr[1].equals("1")){
            	flag = flag + 1;
            }
            totDocs++;
        }
        IOUtils.closeStream(i);
    }
    double accuracy = ((double)flag / (double)totDocs) * 100.0;
    String resString = "\nTesting Results\n---------------------------\nCorrectly Classified: \t" + flag + "\nTotal Documents: \t" + totDocs + "\nPercent Correct: \t" + flag + "/" + totDocs + " = " + Double.toString(accuracy) + "%" ;
//    System.out.println(String.format("%s/%s, accuracy %.2f", flag, totDocs, ((double)flag / (double)totDocs) * 100.0));
    System.out.println(resString);
    
    
    
  }
}

