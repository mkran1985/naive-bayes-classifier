import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */

public class NBclassifierReduce extends
        Reducer<LongWritable, Text, LongWritable, Text> {
    
    private long totDocs;
    private long totUniqueLabels;
    private HashMap<String, Integer> labelStats;
    
    @Override
    protected void setup(Context context) throws IOException {
    	totDocs = context.getConfiguration().getLong(NBmain.totDocs, 100);
        totUniqueLabels = context.getConfiguration().getLong(NBmain.totUniqueLabels, 100);
//        totalDocuments = context.getCounter(NBmain.GlobalCounters.totDocs).getValue();
//        uniqueLabels = context.getCounter(NBmain.GlobalCounters.totUniqueLabels).getValue();
        
        labelStats = new HashMap<String, Integer>();
        
        Path [] files = DistributedCache.getLocalCacheFiles(context.getConfiguration());
        if (files != null || files.length >= 1) {
        	LocalFileSystem fs = FileSystem.getLocal(context.getConfiguration());
	        for (Path file : files) {
	            FSDataInputStream inp = fs.open(file);
	            BufferedReader i = new BufferedReader(new InputStreamReader(inp));
	            String lnStr;
	            while ((lnStr = i.readLine()) != null) {
	                String label = lnStr.split("\\s+")[0];
	                String [] counts = lnStr.split("\\s+")[1].split("-");
	                labelStats.put(label, new Integer(Integer.parseInt(counts[0])));
	            }
	            IOUtils.closeStream(i);
	        }
        }else{
        	   throw new IOException("Distributed Cache Failed!");
        }
     }
    
    @Override
    public void reduce(LongWritable key, Iterable<Text> values, Context context) throws InterruptedException, IOException {
    	HashMap<String, Double> probs = new HashMap<String, Double>();
        ArrayList<String> trueLabels = null;
        
        for (Text val : values) {
            String [] valArr = val.toString().split("::");
//            System.out.println(valArr[0]);
            String [] labelProbs = valArr[0].split(",");
            for (String labelProb : labelProbs) {
                String [] pieces = labelProb.split(":");
                String label = pieces[0];
                double prob = Double.parseDouble(pieces[1]);
                
                probs.put(label, new Double(
                        probs.containsKey(label) ? probs.get(label).doubleValue() + prob : prob));
            }
            
            if (trueLabels == null) {
                String [] list = valArr[1].split(":");
                trueLabels = new ArrayList<String>();
                for (String elem : list) {
                    trueLabels.add(elem);
                }
            }
        }
        
        int m=1;
//        System.out.println("totDoc: "+Long.toString(totDocs));
//        System.out.println("uniqLabels: "+Long.toString(totUniqueLabels));
        
        // To find label that has the highest log likelihood.
        double bestPr = Double.NEGATIVE_INFINITY;
        String bestLab = null;
        for (String l : probs.keySet()) {
            double prior = Math.log((double)labelStats.get(l).intValue()*(double)totUniqueLabels + m ) -
                Math.log((totDocs + m)*(double) totUniqueLabels );
            double totalProb = probs.get(l).doubleValue() + prior;
            if (totalProb > bestPr) {
                bestLab = l;
                bestPr = totalProb;
            }
         }
        int t=0;
        String tLabel = "[";
        for (String lab:trueLabels){
        	tLabel = tLabel.concat(lab + ", ");
        }
        tLabel = tLabel.substring(0, tLabel.length() - 2).concat("]");
        if (tLabel.contains(bestLab)){
        	t=1;
        }
        String resultStr = Integer.toString(t) + "\t" + tLabel + "\t" + bestLab + "\t" +  Double.toString(bestPr);
        context.write(key, new Text(resultStr));
    }

}

