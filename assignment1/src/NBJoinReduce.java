import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */
public class NBJoinReduce extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) 
            throws InterruptedException, IOException {
    	
    	String fromTraining = new String();
    	Vector<String> fromTest = new Vector<String>();
        
    	for (Text v : values) {
    		if (v.toString().contains("-")){
    			fromTraining = v.toString();
    		}else{
    			fromTest.add(v.toString());
    		}
    	}
    		
		if(fromTest.size()>0){
    		if (fromTraining == null) {
                fromTraining = "";
            }
    		StringBuilder output = new StringBuilder();
			output.append(String.format("%s|", fromTraining));
			for (String d : fromTest){
				 output.append(String.format("%s#", d));
			}
		 	String out = output.toString();
			context.write(key, new Text(out.substring(0, out.length()-2)));
    	}	
	}
    	
    	
    	
    	
    	  	  
//        String fromTraining = null;
//        ArrayList<String> dVec = new ArrayList<String>();
//        for (Text v : values) {
//        	String vStr = v.toString();
//            if (vStr.contains("-")) {
//                fromTraining = vStr;
//            } else {
//                dVec.add(vStr);
//            }
//        }
//        
//        if (dVec.size() > 0) {
//            if (fromTraining == null) {
//            	fromTraining = "";
//            }
//            String resStr = new String();
//            for (String doc : dVec) {
//            	resStr = resStr.concat(doc+"#");
//            }
//            context.write(key, new Text(resStr.substring(0, resStr.length() - 1)));
//        }
    }

