import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */
public class NBclassifierMap extends Mapper<Text, Text, LongWritable, Text> {


    private long totUniqueWrd;
    private HashMap<String,Integer> labelStats = new HashMap<String, Integer>();
    
    
    protected void setup(Context context) throws IOException {   
    	totUniqueWrd = context.getConfiguration().getLong(NBmain.totUniqueWords, 100); 
//    	totUniqueWrd = context.getCounter(NBmain.GlobalCounters.totUniqueWords).getValue();
//    	totUniqueLab = context.getCounter(NBmain.GlobalCounters.totUniqueLabels).getValue();
    	
        Path [] files = DistributedCache.getLocalCacheFiles(context.getConfiguration());
        if (files != null || files.length >= 1) {
        	LocalFileSystem fs = FileSystem.getLocal(context.getConfiguration());
	        for (Path file : files) {
	            FSDataInputStream inp = fs.open(file);
	            BufferedReader i = new BufferedReader(new InputStreamReader(inp));
	            String lnStr;
	            while ((lnStr = i.readLine()) != null) {
	                String label = lnStr.split("\\s+")[0];
	                String [] counts = lnStr.split("\\s+")[1].split("-");
	                labelStats.put(label, new Integer(Integer.parseInt(counts[0])));
	            }
	            IOUtils.closeStream(i);
	        }
        }else{
        	   throw new IOException("Distributed Cache Failed!");
        }
     }
    @Override
    public void map(Text key, Text value, Context context) throws InterruptedException, IOException {
    	//Key	Value
        //10	CCAT-3,ECAT-1,MCAT-1,GCAT-8|7481::ECAT,GCAT:1#13557::CCAT,MCAT:1#0::CCAT,MCAT:6
//        System.out.println(key.toString());
    	String valStr = value.toString();
//        System.out.println(valStr);
        String[] valStrArr = {""};
        String trainStr = "";
        String[] trainStrArr = {""};
        String testStr = "";
        String[] testStrArr ={""};
        
        valStrArr = valStr.split("\\|");
        trainStr = valStrArr[0];
        testStr = valStrArr[1];
        
           
       	//Get training counts: <label,count>
        HashMap<String, Integer> trainLabelCounts = new HashMap<String, Integer>();
        if (trainStr.length()>0){
        	trainStrArr = trainStr.split(",");
        	for (String i:trainStrArr){
        		String[] trainLabelCtsArr = i.split("-");
        		trainLabelCounts.put(trainLabelCtsArr[0], Integer.parseInt(trainLabelCtsArr[1]));
        	}
        }
        
        //testStr = "0::CCAT,MCAT:65#16802::CCAT:1"
        
        //testStrArr = {"0::CCAT,MCAT:65" , "16802::CCAT:1"}
        testStrArr = testStr.split("#");
        
        
        //Get test counts (ie. <docID, #wordFreq>)
        HashMap<Long, Integer> testCounts = new HashMap<Long, Integer>();
        
        //Get true label for test data (ie. <docID, #trueLabels>)
        HashMap<Long, String> testTrueLabels = new HashMap<Long, String>();
        
        for (int i=0; i<testStrArr.length; i++){
        	//testStrArr[0] = "0::CCAT,MCAT:65"
        	String[] testDocArr = testStrArr[i].split("::");
        	//testDocArr = {"0" , "CCAT,MCAT:65"}
        	if (testDocArr[1].length()>1){
//        	System.out.println(testDocArr[0]+":"+testDocArr[1]);
        	//docID = "0"
        	String docID = testDocArr[0];
        	//LabelWordFreqArr = {"CCAT,MCAT" , "65"}
        	String[] LabelWordFreqArr = testDocArr[1].split(":");
        	if (LabelWordFreqArr.length > 1){
        	int wordFreq = Integer.parseInt(LabelWordFreqArr[1]);
        	//wordFreq = "65"/
        	testCounts.put(Long.parseLong(docID), wordFreq);
        	String labelList = LabelWordFreqArr[0];
        	testTrueLabels.put(Long.parseLong(docID), labelList);
        	}
        	}
        }
        
        
        int m=1;
//        double q_x = 1/totUniqueWords;
//        double q_y = 1/totUniqueLabels;
//        
        //Iterate through labels to find log likelihood log P(y',w1,w2,....) 
        // 1st Summation
        //labelStats : <label WordCount>
        //CCAT	6627
        //ECAT	3440
        //GCAT	6421
        //MCAT	2437
        
        //testTrueLabels = <docID, LabelList>
        //if "0::CCAT,MCAT:65" -> testTrueLabels = <0, [CCAT,MCAT]>
        
        //iterate through all possible documents in test
        for(Long docID:testTrueLabels.keySet()){
        	String wordProbStr = new String();
        	//iterate through all possible unique labels in training
        	for (String label:labelStats.keySet()){
        		//wlCount is the number of words that appear under "label"
        		int wlCount = labelStats.get(label).intValue();
        		int docWordTestCount = testCounts.get(docID); 
        		int WordLabelTrainCount = 0;
                if (trainLabelCounts != null && trainLabelCounts.containsKey(label)) {
                	WordLabelTrainCount = trainLabelCounts.get(label).intValue();
                }
                double wProb = (double)docWordTestCount * (Math.log(WordLabelTrainCount*totUniqueWrd + m) - Math.log((wlCount + m) * totUniqueWrd));
                wordProbStr = wordProbStr.concat(label +":"+ Double.toString(wProb)+ ",");
        	}
//        	System.out.println("totUniqueWrd ="+Long.toString(totUniqueWrd) );
        	context.write(new LongWritable(docID), 
                    new Text(String.format("%s::%s", wordProbStr.substring(0, wordProbStr.length() - 1), testTrueLabels.get(docID))));
        }
    }
}


