import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */

//[Reference] White-board Discussions with Roi Ceren and Will Richardson 

public class NBLabelStatsReduce extends Reducer<Text, IntWritable, Text, Text> {

  public void reduce(Text key, Iterable<IntWritable> val, Context context)
      throws IOException, InterruptedException {
	  
	  //Now each reducer will receive a particular key (which is a label, in this case)
	  //So we can update the global counter for total unique labels
	  context.getCounter(NBmain.GlobalCounters.totUniqueLabels).increment(1);
	  
	  int sumWPL = 0;
	  int numDocs = 0;
	  for(IntWritable wplIter : val){
		  //The reducer will receive <k,V> i.e. <label,#words in the label>
		  //In each iteration, of this loop we can count how many documents appear with that label 
		  numDocs ++;
		  //Each time we increment numDocs, we can increment the global counter for total number of documents also
		  context.getCounter(NBmain.GlobalCounters.totDocs).increment(1);
		  sumWPL = sumWPL + wplIter.get();	  
	  }
	  
	  //sumWPL = no. of words that appear under a label y (i.e. Y=y and W=*)
	  //numDocs = no. of documents that have a label y (i.e. Y=y)
	  String sumWPLString = Integer.toString(sumWPL) + "-" + Integer.toString(numDocs) ;
      context.write(key, new Text(sumWPLString));	  
  }
}