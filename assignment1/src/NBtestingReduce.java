import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */
public class NBtestingReduce extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) 
            throws InterruptedException, IOException {
    	
        Vector<String> dVec = new Vector<String>();
       
        for (Text value : values) {
        	//value = <docId::L1,L2>
            dVec.add(value.toString());
        }
        
//        int sum = 0;
        HashMap<String,Integer> d_y_count = new HashMap<String, Integer>();
        
        String resStr ="";
        for (String doc : dVec) {
//        	String[] vArr = doc.split("::");
//        	System.out.println(doc);
//        	String docID = vArr[0];
//        	String yStr = vArr[1];
        	
        	if (d_y_count.containsKey(doc) == false){
        		d_y_count.put(doc, 1);
        	}else{
  				d_y_count.put(doc, d_y_count.get(doc).intValue() + 1);
  			}
        	//resStr = resStr.concat(doc);//+":=:".concat(Integer.toString(sum)).concat(" "));
        }
        
        
        for (String s : d_y_count.keySet()){
//        	String[] vArr = s.split("::");
//        	String docID = vArr[0];
//        	String yStr = vArr[1];
        	resStr = resStr.concat(s).concat(":").concat(Integer.toString(d_y_count.get(s).intValue()).concat("#"));
        }
        
        context.write(key, new Text(resStr.substring(0, resStr.length())));
    }
}
