import java.io.IOException;
import java.util.Vector;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */

public class NBtrainingMap extends Mapper<LongWritable, Text, Text, Text> {

  @Override
  public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
	  
	  //convert documents (each line/value) into words
	  String[] wArr = value.toString().split("\\s+"); 
	  Vector<String> wVec = NBmain.removeNonWords(wArr);
	  Vector<String> yVec = NBmain.removeCommas(wArr[0]);
	  
	  // (Y = y, W = w)
	  for (String y:yVec){
		  if (y.matches(".*CAT.*")){
			  for (String w : wVec){
	                context.write(new Text(w), new Text(y));
	            }
		  }
	  }
  }
}
