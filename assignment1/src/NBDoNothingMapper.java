import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */
public class NBDoNothingMapper extends Mapper<Text, Text, Text, Text> {

    @Override
    public void map(Text key, Text value, Context context) throws InterruptedException, IOException {
        context.write(key, value);
    }
}
