import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Assignment 1
 * @author Muthukumaran Chandrasekaran
 * Naive Bayes Classifier
 * Current Version: v0.5
------------------------------------
 */

public class NBtrainingReduce extends Reducer<Text, Text, Text, Text> {

  public void reduce(Text key, Iterable<Text> values, Context context)
      throws IOException, InterruptedException {
	  
	  	  
//	  Pseudo Code from assignment1.pdf
//	  --------------------------------------------
//	  for each example {y [w1,...,wN]}:{
//		  increment #(Y=y) by 1
//		  increment #(Y=*) by 1
//	  }
//	  for i=i to N:{
//		  increment #(Y=y,W=w) by 1
//		  increment #(Y=y,W=*) by N
//	  }
	  
	  //Now each reducer will receive a particular key (which is a word, in this case)
	  //So we can update the global counter for total unique words
	  context.getCounter(NBmain.GlobalCounters.totUniqueWords).increment(1);
	  
	  //Now we would like to count the number of times a word w appears for a particular label y
	  HashMap<String,Integer> y_w_count = new HashMap<String, Integer>();
	  
	  for(Text yIter : values){
		  String yStr = yIter.toString();
		  if (y_w_count.containsKey(yStr) == false){
			  y_w_count.put(yStr, 1);	
		  }else{
			  y_w_count.put(yStr, y_w_count.get(yStr).intValue() + 1);
		  }
	  }
	  
      String y_wStr = new String(); 
      for (String yStr : y_w_count.keySet()) {
          y_wStr = y_wStr.concat(yStr + "-" + y_w_count.get(yStr).toString()+",");
          
      }
	  
      context.write(key, new Text(y_wStr.substring(0, y_wStr.length() - 1)));
	  
  }
}